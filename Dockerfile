FROM --platform=linux/amd64 kasmweb/ubuntu-focal-dind-rootless:1.13.1

USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########


COPY artifacts/ /harbinger/
COPY README.md /harbinger/
COPY harbinger-dev-workspace-setup.sh /harbinger/
# COPY artifacts/vsix/* /harbinger/vsix/

RUN chmod +x /harbinger/harbinger-dev-workspace-setup.sh && \
    chown -R 1000:0 /harbinger/

RUN apt-get update && apt-get install -y \
    libasound2 \
    libegl1-mesa \
    libgl1-mesa-glx \
    libxcomposite1 \
    libxcursor1 \
    libxi6 \
    libxrandr2 \
    libxrandr2 \
    libxss1 \
    libxtst6 \
    && cd /tmp/ && wget https://repo.anaconda.com/archive/Anaconda3-2023.07-1-Linux-x86_64.sh \
    && bash Anaconda3-20*-Linux-x86_64.sh -b -p /opt/anaconda3 \
    && rm -r /tmp/Anaconda3-20*-Linux-x86_64.sh \
    && echo 'source /opt/anaconda3/bin/activate' >> /etc/bash.bashrc \
    # Update all the conda things
    && bash -c "source /opt/anaconda3/bin/activate \
    && conda update -n root conda  \
    && conda update --all \
    && conda clean --all" \
    && /opt/anaconda3/bin/conda config --set ssl_verify /etc/ssl/certs/ca-certificates.crt \
    && /opt/anaconda3/bin/conda install pip \
    && mkdir -p /home/kasm-user/.pip \
    && chown -R 1000:1000 /opt/anaconda3 /home/kasm-default-profile/.conda/

RUN unzip /harbinger/awscliv2.zip -d /opt/awscliv2 \
    && /opt/awscliv2/aws/install \
    && gzip -d /harbinger/pycharm-community-2023.2.tar.gz \
    && mkdir -p /opt/pycharm/ \
    && tar -xvf /harbinger/pycharm-community-2023.2.tar -C /opt/pycharm/ \
    && mkdir -p $HOME/.local/share/applications/ \
    && mv /harbinger/pycharm.desktop $HOME/Desktop \
    && ls -lisah /harbinger \
    && mv /harbinger/images /opt/images \
    && rm -rf /harbinger/* \
    && ls -lisah /opt/ \
    && ls -lisah /opt/images/

RUN mv /opt/images/desktop-background-1.jpg /usr/share/extra/backgrounds/bg_default.png


# RUN ls -lisah /artifacts/
# RUN ls -lisah $HOME/Desktop

##### Install Tooling

#### Prerequisites

### AWS CA Bundle

### Harbinger CA Bundle

#### CLI

### Anaconda3 silent install (no prompts) with --batch --force --path 

# ~5-10 minutes for unpack and install
# RUN chmod +x /artifacts/Anaconda3-2023.07-1-Linux-x86_64.sh && \
#     /artifacts/Anaconda3-2023.07-1-Linux-x86_64.sh -b -f -p $HOME/anaconda3/ && \
#     $HOME/anaconda3/bin/conda init bash && \
#     unzip /artifacts/awscliv2.zip -d $HOME/awscliv2 && \
#     $HOME/awscliv2/aws/install && \
#     gzip -d /artifacts/pycharm-community-2023.2.tar.gz && \
#     tar -xvf /artifacts/pycharm-community-2023.2.tar -C $HOME && \
#     mkdir -p $HOME/.local/share/applications/ && \
#     mv /artifacts/pycharm.desktop $HOME/.local/share/applications/ && \
#     dpkg -i /artifacts/drawio-amd64-21.6.5.deb && \
#     wget 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64' -O /tmp/code_latest_amd64.deb && \
#     dpkg -i /tmp/code_latest_amd64.deb && \
#     mkdir -p $HOME/.vscode/extensions && \
#     chmod -R 777 $HOME/.vscode/
# USER 1000
# RUN for extension in $(ls /artifacts/vsix/); do code --install-extension /artifacts/vsix/$extension ; done
# RUN mkdir -p -m 775 $HOME/.config/Code


# Successful Installs 
# RUN code --install-extension /artifacts/vsix/HashiCorp.terraform-2.27.2023071109@linux-x64.vsix && \
#     code --install-extension /artifacts/vsix/SonarSource.sonarlint-vscode-3.19.2@linux-x64.vsix && \
#     code --install-extension /artifacts/vsix/batisteo.vscode-django-1.10.0.vsix && \
#     code --install-extension /artifacts/vsix/hediet.vscode-drawio-1.6.6.vsix && \
#     code --install-extension /artifacts/vsix/ms-azuretools.vscode-docker-1.26.0.vsix && \
#     code --install-extension /artifacts/vsix/redhat.vscode-yaml-1.14.0.vsix && \
#     code --install-extension /artifacts/vsix/DavidAnson.vscode-markdownlint-0.51.0.vsix && \
#     code --install-extension /artifacts/vsix/ms-vscode.makefile-tools-0.8.4.vsix && \
#     code --install-extension /artifacts/vsix/tomoki1207.pdf-1.2.2.vsix && \
#     code --install-extension /artifacts/vsix/yzane.markdown-pdf-1.4.4.vsix && \
#     code --install-extension /artifacts/vsix/ms-python.python-2023.8.0.vsix

# Nice to have but pressed for time and these are tedious
# code --install-extension /artifacts/vsix/esbenp.prettier-vscode-9.19.0.vsix && \
# code --install-extension /artifacts/vsix/mikestead.dotenv-1.0.1.vsix

# RUN code --install-extension /artifacts/vsix/ms-mssql.sql-database-projects-vscode-1.2.0.vsix  || echo "Failed"
# RUN code --install-extension /artifacts/vsix/ms-toolsai.jupyter-2023.7.1002091100@linux-x64.vsix || echo "Failed"
# RUN code --install-extension /artifacts/vsix/golang.Go-0.39.1.vsix || echo "Failed"
# RUN code --install-extension /artifacts/vsix/ms-kubernetes-tools.vscode-kubernetes-tools-1.3.13.vsix || echo "Failed"
# RUN code --install-extension /artifacts/vsix/ms-vscode-remote.remote-ssh-0.105.2023072715.vsix || echo "Failed"


# Failed Installs
# --no-sandbox --user-data-dir $HOME/.config/Code; done

USER root



######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000
