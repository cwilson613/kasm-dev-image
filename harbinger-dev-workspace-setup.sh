#!/bin/bash
# CRW 01-Aug-2023

# ~5-10 minutes for unpack and install

# Make Anaconda Installer Script Executable
chmod +x /artifacts/Anaconda3-2023.07-1-Linux-x86_64.sh

/artifacts/Anaconda3-2023.07-1-Linux-x86_64.sh -b -f -p $HOME/anaconda3/ &&
    $HOME/anaconda3/bin/conda init bash &&
    unzip /artifacts/awscliv2.zip -d $HOME/awscliv2 &&
    $HOME/awscliv2/aws/install &&
    gzip -d /artifacts/pycharm-community-2023.2.tar.gz &&
    tar -xvf /artifacts/pycharm-community-2023.2.tar -C $HOME &&
    mkdir -p $HOME/.local/share/applications/ &&
    mv /artifacts/pycharm.desktop $HOME/.local/share/applications/ &&
    dpkg -i /artifacts/drawio-amd64-21.6.5.deb &&
    wget 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64' -O /tmp/code_latest_amd64.deb &&
    dpkg -i /tmp/code_latest_amd64.deb &&
    mkdir -p $HOME/.vscode/extensions &&
    chmod -R 777 $HOME/.vscode/
# USER 1000
# RUN for extension in $(ls /artifacts/vsix/); do code --install-extension /artifacts/vsix/$extension ; done
# RUN mkdir -p -m 775 $HOME/.config/Code

# Successful Installs
# RUN code --install-extension /artifacts/vsix/HashiCorp.terraform-2.27.2023071109@linux-x64.vsix && \
#     code --install-extension /artifacts/vsix/SonarSource.sonarlint-vscode-3.19.2@linux-x64.vsix && \
#     code --install-extension /artifacts/vsix/batisteo.vscode-django-1.10.0.vsix && \
#     code --install-extension /artifacts/vsix/hediet.vscode-drawio-1.6.6.vsix && \
#     code --install-extension /artifacts/vsix/ms-azuretools.vscode-docker-1.26.0.vsix && \
#     code --install-extension /artifacts/vsix/redhat.vscode-yaml-1.14.0.vsix && \
#     code --install-extension /artifacts/vsix/DavidAnson.vscode-markdownlint-0.51.0.vsix && \
#     code --install-extension /artifacts/vsix/ms-vscode.makefile-tools-0.8.4.vsix && \
#     code --install-extension /artifacts/vsix/tomoki1207.pdf-1.2.2.vsix && \
#     code --install-extension /artifacts/vsix/yzane.markdown-pdf-1.4.4.vsix && \
#     code --install-extension /artifacts/vsix/ms-python.python-2023.8.0.vsix
