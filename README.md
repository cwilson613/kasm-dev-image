# Harbinger Development Workspace Image

Built for use with KASM Workspaces, a Containerized Desktop Infrastructure (CDI) platform.

Instead of VMs, which require all the overhead of a hypervisor and emulation layer, we use containers, which will use the Linux Kernel present on the KASM Workspaces machine.

Each Workspace has its own "Persistent Profile", which is the `$HOME` directory inside the workspace: `/home/kasm_user/`.  Each user in KASM has their own profile mapped when starting an image, meaning your files and programs installed/stored in your `$HOME` directory will follow you and persist between sessions.

## First Time Setup

If this is your first time using the Harbinger Dev Workspace, the following steps will get the pre-bundled artifacts installed to your persistent profile:

Open a terminal and run:

```bash
~/Desktop/harbinger-dev-workspace-setup.sh
```
