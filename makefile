GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
ANACONDA_URL := "https://repo.anaconda.com/archive/"
ANACONDA_FILE := "Anaconda3-2023.07-1-Linux-x86-64.sh"

AWSCLI_URL := "https://awscli.amazonaws.com/"

DEV_IMAGE := kasmweb/harbinger-dev:$(GIT_BRANCH)

.PHONY: artifacts
artifacts:
	# Anaconda3
	wget $(ANACONDA_URL)/$(ANACONDA_FILE) -P artifacts/
	# AWS CLI
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "artifacts/awscliv2.zip"
	# PyCharm IDE
	wget https://download.jetbrains.com/python/pycharm-community-2023.2.tar.gz -P artifacts/
	# Draw.io
	wget https://github.com/jgraph/drawio-desktop/releases/download/v21.6.5/drawio-amd64-21.6.5.deb -P artifacts/
	
.PHONY: build
build:
	docker build --progress=plain -t $(DEV_IMAGE) .

.PHONY: run
	docker run --platform=linux/amd64 --rm -it --user root --shm-size=512m -p 6901:6901 -e VNC_PW=password $(DEB_IMAGE)
